const fs = require('fs')
const express = require("express")
const brain = require("brain.js")

const app = express()
const port = "8000"

// Neural Network setup and training
const network = new brain.recurrent.LSTM()
var trainingConf = {iterations: 1500, log: true, logPeriod: 50, errorThresh: 0.01, layers: [10]}
var trainingSet = JSON.parse(fs.readFileSync('training_set/dataset.json', 'utf8'))
network.train(trainingSet, trainingConf)

// Get requests
app.get("/", (req, res) => {
  // http://localhost:8000?arg0=Word
  var arg0 = req.query.arg0
  res.json({
    output: network.run(arg0)
  })
})

// Listen to requests on port 8000
app.listen(port, () => {
  console.log("Listening on port " + port)
})
